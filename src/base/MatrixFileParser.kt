package base

import matrixLib.Matrix
import matrixLib.createMatrix
import java.io.File
import java.util.*

class MatrixFileParser {

    companion object {
        fun readMatrixFromFile(): ArrayList<ArrayList<Int>> {
            val array = ArrayList<ArrayList<Int>>()
            val input = Scanner(File("src/files/$FILE_NAME.txt"))
            while (input.hasNextLine()) {
                val colReader = Scanner(input.nextLine())
                val row = ArrayList<Int>()
                while (colReader.hasNextInt()) {
                    row.add(colReader.nextInt())
                }
                array.add(row)
            }
//        println("Main Array read from .txt file:  $array")
            return array
        }

        fun divideMatrix(matrix: ArrayList<ArrayList<Int>>): Pair<Matrix<Int>, Matrix<Int>> {
            //first array in file
            val distanceArray = ArrayList<ArrayList<Int>>()
            //second array in file
            val flowArray = ArrayList<ArrayList<Int>>()
            val sampleEmptyArray = ArrayList<Int>()
            var isFirstMatrix = true

            for (i in 0..(matrix.size - 1)) {
                if (matrix[i] == sampleEmptyArray)
                    isFirstMatrix = false
                else if (isFirstMatrix)
                    distanceArray.add(matrix[i])
                else
                    flowArray.add(matrix[i])
            }
            return parseToMatrix(distanceArray, flowArray)
        }

        private fun parseToMatrix(distanceArray: ArrayList<ArrayList<Int>>, flowArray: ArrayList<ArrayList<Int>>): Pair<Matrix<Int>, Matrix<Int>> {
            val matrixDistance = createMatrix(MATRIX_ROW_COUNT, MATRIX_ROW_COUNT) { x, y ->
                distanceArray[y][x]
            }
            val matrixFlow = createMatrix(MATRIX_ROW_COUNT, MATRIX_ROW_COUNT) { x, y ->
                flowArray[y][x]
            }
            println("Distance Matrix: $matrixDistance")
            println("Flow Matrix: $matrixFlow")
            return Pair(matrixDistance, matrixFlow)
        }

    }

}