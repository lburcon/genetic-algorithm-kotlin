package base

import genetic.PopulationManager
import greedy.GreedyPopulationManager
import matrixLib.Matrix
import random.RandomPopulationManager
import kotlin.collections.ArrayList

const val MATRIX_ROW_COUNT = 12
const val FILE_NAME = "had$MATRIX_ROW_COUNT"
lateinit var mainArray: ArrayList<ArrayList<Int>>
lateinit var matrixDistance: Matrix<Int>
lateinit var matrixFlow: Matrix<Int>

fun main(args: Array<String>) {
    prepareMatrixes()
    startGeneticAlgorythm()
//    startRandom()
//    startGreedy()
}

fun startGreedy() {
    val populationManager = GreedyPopulationManager()
    populationManager.startLifecycle()
}

fun startRandom() {
    val populationManager = RandomPopulationManager()
    populationManager.startLifecycle()
}

fun startGeneticAlgorythm() {
    val populationManager = PopulationManager()
    populationManager.startLifecycle()
}

private fun prepareMatrixes() {
    mainArray = MatrixFileParser.readMatrixFromFile()
    val matrixPair = MatrixFileParser.divideMatrix(mainArray)

    matrixDistance = matrixPair.first
    matrixFlow = matrixPair.second
}
