package base

class Environment {
    companion object {
        const val POP_SIZE = 100
        const val GENERATIONS = 100
        const val GREEDY_COUNT = 100
        const val CROSSING_POSSIBILITY = 0.7
        const val MUTATION_POSSIBILITY = 0.01
        const val TOURNAMENT_COUNT = 5
    }
}