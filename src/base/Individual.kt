package base

import java.util.*
import java.util.Random


class Individual(var permutationArray: ArrayList<Int>) {

    companion object {
        private val random = Random()

        fun generateArrayWithoutRepetitions(numberCount: Int): ArrayList<Int> {
            val arrayIndividual = ArrayList<Int>()

            val listWithRowNumbers = ArrayList<Int>(numberCount)
            for (i in 0 until numberCount)
                listWithRowNumbers.add(i)

            for (i in 0 until numberCount) {
                val randomIndex = random.nextInt(listWithRowNumbers.size)
                val randomElement = listWithRowNumbers[randomIndex]
                arrayIndividual.add(randomElement)
                listWithRowNumbers.removeAt(randomIndex)
            }
            return arrayIndividual
        }

        fun countFitness(individual: Individual): Int {
            var cost = 0
            for (i in 0 until individual.permutationArray.size) {
                for (j in 0 until individual.permutationArray.size) {
                    val localizationFirst = individual.permutationArray[i]
                    val localizationSecond = individual.permutationArray[j]
                    val distance = matrixDistance[localizationFirst, localizationSecond]
                    val flow = matrixFlow[i, j]
                    cost += distance * flow
                }
            }
            return cost
        }

        fun countFitness(permutationArray: ArrayList<Int>): Int {
            var cost = 0
            for (i in 0 until permutationArray.size) {
                for (j in 0 until permutationArray.size) {
                    val localizationFirst = permutationArray[i]
                    val localizationSecond = permutationArray[j]
                    val distance = matrixDistance[localizationFirst, localizationSecond]
                    val flow = matrixFlow[i, j]
                    cost += distance * flow
                }
            }
            return cost
        }

        fun prepareRandomIndividual(): Individual {
            return Individual(generateArrayWithoutRepetitions(MATRIX_ROW_COUNT))
        }

    }


}