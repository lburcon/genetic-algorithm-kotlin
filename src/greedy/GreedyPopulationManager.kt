package greedy

import base.Environment
import base.Individual
import java.util.*

class GreedyPopulationManager {
    companion object {
        val newPopulationArray = ArrayList<Individual>()
        var count = 0
        var bestFitness = 10000
        var bestIndividual = Individual.prepareRandomIndividual()

    }

    fun startLifecycle() {
        while (count < Environment.GREEDY_COUNT) {
            prepareNewPopulation()
            displayBest()
            count++
        }
    }


    private fun prepareNewPopulation() {

        while (newPopulationArray.size < Environment.POP_SIZE) {

            //start selecting and add best member to new population
            val newIndividual: Individual = GreedyAlgorithm.startAlgorythm()

            //make new instance of base.Individual to handle case with the same objects
            addToNewPopulation(newIndividual)
        }

    }

    /**
     * Making new object with same values just to be sure that we won't have two same objects in our population,
     * which would break my code, which depends on array references
     */
    private fun addToNewPopulation(newIndividual: Individual) {

        if (Individual.countFitness(newIndividual) < bestFitness) {
            bestFitness = Individual.countFitness(newIndividual)
            println("CHANGE: BEST: $bestFitness")
            bestIndividual = newIndividual
        }

    }

    fun displayBest() {
        println("GENERATIONS: $count $bestFitness, PERMUTATION: ${bestIndividual.permutationArray}")
    }
}

