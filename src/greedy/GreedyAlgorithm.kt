package greedy

import base.Individual
import base.MATRIX_ROW_COUNT
import java.util.*

class GreedyAlgorithm {
    companion object {

        fun startAlgorythm(): Individual {
            val individual = Individual(ArrayList())
            val permutation = individual.permutationArray
            var greedyArray = ArrayList<Int>()
            var bestCost = 10000

            permutation.add(Random().nextInt(MATRIX_ROW_COUNT))

            for (i in 0 until MATRIX_ROW_COUNT) {
                var index = -1
                for (j in 0 until MATRIX_ROW_COUNT) {

                    if (!permutation.contains(j)) {
                        greedyArray.addAll(permutation)
                        greedyArray.add(j)
                        val tempCost = Individual.countFitness(greedyArray)
                        if (tempCost < bestCost) {
                            bestCost = tempCost
                            index = j
                        }
                        greedyArray = ArrayList()

                    }

                }
                bestCost = 10000
                if (index != -1)
                permutation.add(index)
                greedyArray = ArrayList()

            }
            return individual

        }
    }
}