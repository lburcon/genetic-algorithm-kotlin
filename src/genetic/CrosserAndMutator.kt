package genetic

import base.Environment
import java.util.*
import base.Individual
import base.MATRIX_ROW_COUNT

class CrosserAndMutator {

    companion object {

        //checks if random from 0..100 is >= 30, which gives 70% possibility ratio
        private fun shouldCrossingHappen(): Boolean {
            val probability = PopulationManager.random.nextInt(100 + 1)
            return probability >= (100 - (Environment.CROSSING_POSSIBILITY * 100))
        }

        //checks if random from 0..100 is >= 99, which gives 1% possibility ratio
        private fun shouldMutationHappen(): Boolean {
            val probability = PopulationManager.random.nextInt(100 + 1)
            return probability >= (100 - (Environment.MUTATION_POSSIBILITY * 100))
        }

        fun startCrossingAndMutation() {
            val tempArray = ArrayList<Individual>()
            while (PopulationManager.newPopulationArray.size >= 2) {
                val firstIndividual = PopulationManager.newPopulationArray.removeAt(0)
                val secondIndividual = PopulationManager.newPopulationArray.removeAt(0)

                if (shouldCrossingHappen()) {
                    crossing(firstIndividual, secondIndividual)
                }
                mutation(firstIndividual, secondIndividual)

                tempArray.add(firstIndividual)
                tempArray.add(secondIndividual)
            }
            PopulationManager.populationArray = ArrayList()
            PopulationManager.populationArray.addAll(tempArray)

        }

        private fun mutation(firstIndividual: Individual, secondIndividual: Individual) {
            for (i in 0 until MATRIX_ROW_COUNT) {

                if (shouldMutationHappen()) {
                    val secondRandomIndex = PopulationManager.random.nextInt(MATRIX_ROW_COUNT)
                    val firstArray = firstIndividual.permutationArray
                    val secondArray = secondIndividual.permutationArray

                    Collections.swap(firstArray, i, secondRandomIndex)
                    Collections.swap(secondArray, i, secondRandomIndex)
                }
            }
        }

        private fun crossing(firstIndividual: Individual, secondIndividual: Individual) {
            //get crossing index
            val arraysSize = firstIndividual.permutationArray.size
            var crossIndex = PopulationManager.random.nextInt(arraysSize)
            if (crossIndex == 0)
                crossIndex++
            else if (crossIndex == 11)
                crossIndex--

            val arraysPair = divideGenes(crossIndex, firstIndividual.permutationArray, secondIndividual.permutationArray)

            firstIndividual.permutationArray = ArrayList(arraysPair.first)
            secondIndividual.permutationArray = ArrayList(arraysPair.second)
            checkAndRepairPermutation(firstIndividual)
            checkAndRepairPermutation(secondIndividual)
        }

        private fun divideGenes(crossIndex: Int, firstPermutationArray: ArrayList<Int>,
                                secondPermutationArray: ArrayList<Int>): Pair<ArrayList<Int>, ArrayList<Int>> {
            val newFirstArray = ArrayList<Int>()
            val newSecondArray = ArrayList<Int>()

            for (i in 0 until crossIndex) {
                newFirstArray.add(secondPermutationArray.removeAt(0))
                newSecondArray.add(firstPermutationArray.removeAt(0))
            }
            newFirstArray.addAll(firstPermutationArray)
            newSecondArray.addAll(secondPermutationArray)
            return Pair(newFirstArray, newSecondArray)
        }

        private fun checkAndRepairPermutation(individual: Individual) {
            var set = HashSet<Int>(individual.permutationArray)
            while (set.size != MATRIX_ROW_COUNT) {
                repairPermutation(individual.permutationArray, set)
                set = HashSet(individual.permutationArray)
            }
        }

        private fun repairPermutation(permutationArray: ArrayList<Int>, set: HashSet<Int>) {
            val usedInts = ArrayList<Int>(set)
            val arrayOfAllInts = Individual.generateArrayWithoutRepetitions(MATRIX_ROW_COUNT)
            val needUsageInts = ArrayList<Int>()

            //check which numbers we lack and which indexes are doubled
            for (i in 0 until arrayOfAllInts.size) {
                val number = arrayOfAllInts[i]
                if (!usedInts.contains(number)) {
                    needUsageInts.add(number)
                }
            }

            val doubledNumbersArray = HashSet<Int>()
            for (i in 0 until MATRIX_ROW_COUNT) {
                for (j in 0 until MATRIX_ROW_COUNT) {
                    if (i != j && permutationArray[i] == permutationArray[j])
                        doubledNumbersArray.add(permutationArray[i])
                }
            }

            for (i in 0 until permutationArray.size) {
                if (doubledNumbersArray.contains(permutationArray[i])) {
                    permutationArray.removeAt(i)
                    permutationArray.add(i, needUsageInts[0])
                    //this number is used right now
                    needUsageInts.remove(0)
                    break
                }
            }

        }
    }
}