package genetic

import base.Environment
import java.util.*
import kotlin.collections.ArrayList
import base.Individual

class PopulationManager {

    companion object {
        var populationArray = ArrayList<Individual>()
        val newPopulationArray = ArrayList<Individual>()
        @JvmField
        val random = Random()
        var count = 0
        //whether we want tournament or roulette
        var SELECT_BY_TOURNAMENT = true
        var bestFitness = 10000
        var worstFitness = 0
        var fitnessSum = 0
        var bestIndividual = Individual.prepareRandomIndividual()
        var worstIndividual = Individual.prepareRandomIndividual()

    }

    fun startLifecycle() {
        prepareFirstPopulation()
        while (count < Environment.GENERATIONS) {
            displayBest()
            prepareNewPopulation()
            count++
        }
    }

    private fun prepareFirstPopulation() {
        for (i in 0 until Environment.POP_SIZE) {
            populationArray.add(Individual.prepareRandomIndividual())
        }
    }

    private fun prepareNewPopulation() {
        TournamentOrganiser.updateTournamentOrganiser()
        RouletteOrganiser.updateRouletteOrganizer()

        //add selected individuals from populationArray to newPopulationArray, then
        //cross and mute them, and add to populationArray again
        while (newPopulationArray.size < Environment.POP_SIZE) {

            //start selecting and add best member to new population
            val tournamentWinner: Individual = if (SELECT_BY_TOURNAMENT)
                TournamentOrganiser.startTournament()
            else
                RouletteOrganiser.startRoulette()

            //make new instance of base.Individual to handle case with the same objects
            addToNewPopulation(tournamentWinner)
        }

        CrosserAndMutator.startCrossingAndMutation()
    }

    /**
     * Making new object with same values just to be sure that we won't have two same objects in our population,
     * which would break my code, which depends on array references
     */
    private fun addToNewPopulation(tournamentWinner: Individual) {

        if (Individual.countFitness(tournamentWinner) < bestFitness) {
            bestFitness = Individual.countFitness(tournamentWinner)
            bestIndividual = tournamentWinner
        }
        if (Individual.countFitness(tournamentWinner) > worstFitness) {
            worstFitness = Individual.countFitness(tournamentWinner)
            worstIndividual = tournamentWinner
        }

        val winnerArray = ArrayList<Int>()
        winnerArray.addAll(tournamentWinner.permutationArray)
        val newIndividual = Individual(winnerArray)
        newPopulationArray.add(newIndividual)

    }

    fun displayBest() {

        for (i in 0 until populationArray.size) {
            fitnessSum += Individual.countFitness(populationArray[i])
        }
        val avgFitness = fitnessSum / populationArray.size

        println("$bestFitness, $avgFitness, $worstFitness")
//        println("GENERATION: $count, BEST: $bestFitness, AVG: $avgFitness, WORST: $worstFitness")
        bestFitness = 10000
        worstFitness = 0
        fitnessSum = 0
    }

}



