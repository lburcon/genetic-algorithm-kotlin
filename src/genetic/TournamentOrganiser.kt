package genetic

import base.Environment
import java.util.*
import base.Individual

class TournamentOrganiser {

    companion object {
        private lateinit var populationArray: ArrayList<Individual>
        private lateinit var randomNumberArray: ArrayList<Int>
        private val random = Random()

        fun updateTournamentOrganiser() {
            populationArray = PopulationManager.populationArray
        }

        //select best member
        fun startTournament(): Individual {
            val tournamentMembers = getTournamentMembers()

            var bestMemberIndex = 0
            var bestFitnessScore = Individual.countFitness(tournamentMembers[0])
            for (i in 1 until Environment.TOURNAMENT_COUNT) {
                //the less, the better
                if (Individual.countFitness(tournamentMembers[i]) < bestFitnessScore) {
                    bestFitnessScore = Individual.countFitness(tournamentMembers[i])
                    bestMemberIndex = i
                }
            }
            return tournamentMembers[bestMemberIndex]
        }

        //select randomly 5 tournament members
        private fun getTournamentMembers(): ArrayList<Individual> {
            initRandomNumberArray()

            val tournamentMemberArray = ArrayList<Individual>(Environment.TOURNAMENT_COUNT)
            for (i in 0 until Environment.TOURNAMENT_COUNT) {
                val randomIndex = randomNumberArray[i]
                tournamentMemberArray.add(populationArray[randomIndex])
            }
            return tournamentMemberArray
        }

        private fun initRandomNumberArray() {
            randomNumberArray = ArrayList()
            for (i in 0 until Environment.TOURNAMENT_COUNT)
                randomNumberArray.add(random.nextInt(Environment.POP_SIZE))
        }
    }

}