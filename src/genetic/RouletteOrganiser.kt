package genetic

import base.Environment
import java.util.*
import base.Individual

class RouletteOrganiser {

    companion object {
        private lateinit var populationArray: ArrayList<Individual>
        private var rouletteArray = ArrayList<Double>()
        private val fitnessArray = ArrayList<Int>()
        private var fitnessCount = 0.0
        private const val POW = 10.0

        fun updateRouletteOrganizer() {
            fitnessCount = 0.0
            rouletteArray = ArrayList()
            countPopulationFitness()
            preparePercentageRoulette()
        }

        fun startRoulette(): Individual {
            val randomDouble = PopulationManager.random.nextDouble() * 100
            var index = 0

            for (i in 0 until Environment.POP_SIZE) {
                if (rouletteArray[i] > randomDouble) {
                    index = i
                    break
                }
            }

            return populationArray[index]
        }

        private fun countPopulationFitness() {
            populationArray = PopulationManager.populationArray

            for (i in 0 until Environment.POP_SIZE) {
                fitnessCount += Math.pow(1 / Individual.countFitness(populationArray[i]).toDouble(), POW)
            }
        }

        private fun preparePercentageRoulette() {
            var fitnessPercentage = 0.0
            for (i in 0 until Environment.POP_SIZE) {
                val fitnessIndividual = Individual.countFitness(populationArray[i])
                fitnessPercentage += (Math.pow((1 / fitnessIndividual.toDouble()), POW) / fitnessCount * 100)
                rouletteArray.add(fitnessPercentage)
                fitnessArray.add(fitnessIndividual)
            }
        }
    }

}