package random

import base.Environment
import base.Individual
import genetic.PopulationManager
import greedy.GreedyPopulationManager.Companion.bestFitness
import java.util.*

class RandomPopulationManager {

    companion object {
        var populationArray = ArrayList<Individual>()
        val newPopulationArray = ArrayList<Individual>()
        @JvmField
        val random = Random()
        var count = 0
        var worstFitness = 0
        var fitnessSum = 0
        var bestIndividual = Individual.prepareRandomIndividual()
        var worstIndividual = Individual.prepareRandomIndividual()

    }

    fun startLifecycle() {
        prepareFirstPopulation()
        while (count < Environment.GENERATIONS) {
            prepareNewPopulation()
            displayBest()
            count++
        }
    }

    private fun prepareFirstPopulation() {
        for (i in 0 until Environment.POP_SIZE) {
            populationArray.add(Individual.prepareRandomIndividual())
        }
    }

    private fun prepareNewPopulation() {

        while (newPopulationArray.size < Environment.POP_SIZE) {

            //start selecting and add best member to new population
            val newIndividual: Individual = Individual.prepareRandomIndividual()

            //make new instance of base.Individual to handle case with the same objects
            addToNewPopulation(newIndividual)
        }

        //always mute to create random population
        MutatorRandom.startCrossingAndMutation()
    }

    /**
     * Making new object with same values just to be sure that we won't have two same objects in our population,
     * which would break my code, which depends on array references
     */
    private fun addToNewPopulation(newIndividual: Individual) {

        if (Individual.countFitness(newIndividual) < bestFitness) {
            bestFitness = Individual.countFitness(newIndividual)
            bestIndividual = newIndividual
        }
        if (Individual.countFitness(newIndividual) > worstFitness) {
            worstFitness = Individual.countFitness(newIndividual)
            worstIndividual = newIndividual
        }

        val winnerArray = ArrayList<Int>()
        winnerArray.addAll(newIndividual.permutationArray)
        val newIndividualObject = Individual(winnerArray)
        newPopulationArray.add(newIndividualObject)
    }

    fun displayBest() {
        for (i in 0 until populationArray.size) {
            fitnessSum += Individual.countFitness(populationArray[i])
        }
        val avgFitness = fitnessSum / populationArray.size

        println("$bestFitness, $avgFitness, $worstFitness")
//        println("GENERATION: $count, BEST: $bestFitness, AVG: $avgFitness, WORST: $worstFitness")
        worstFitness = 0
        fitnessSum = 0
    }

}