package random

import base.Individual
import base.MATRIX_ROW_COUNT
import java.util.*

/**
 * Class for returning random new population
 */
class MutatorRandom {

    companion object {

        fun startCrossingAndMutation() {
            val tempArray = ArrayList<Individual>()
            while (RandomPopulationManager.newPopulationArray.size >= 2) {
                val firstIndividual = RandomPopulationManager.newPopulationArray.removeAt(0)
                val secondIndividual = RandomPopulationManager.newPopulationArray.removeAt(0)

                mutation(firstIndividual, secondIndividual)

                tempArray.add(firstIndividual)
                tempArray.add(secondIndividual)
            }
            RandomPopulationManager.populationArray = ArrayList()
            RandomPopulationManager.populationArray.addAll(tempArray)

        }

        private fun mutation(firstIndividual: Individual, secondIndividual: Individual) {
            for (i in 0 until MATRIX_ROW_COUNT) {

                val secondRandomIndex = RandomPopulationManager.random.nextInt(MATRIX_ROW_COUNT)
                val firstArray = firstIndividual.permutationArray
                val secondArray = secondIndividual.permutationArray

                Collections.swap(firstArray, i, secondRandomIndex)
                Collections.swap(secondArray, i, secondRandomIndex)
            }
        }

    }
}